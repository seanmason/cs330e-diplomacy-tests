
from io import StringIO
from unittest import main, TestCase

#import all relevant functions and test them here
#input is ist of strings or a list of lists?
from Diplomacy import diplomacy_read , diplomacy_solve , diplomacy_final , diplomacy_print 


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):


    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        i, j , k = diplomacy_read(s)
        self.assertEqual(i,  "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")

    # corner cases : valid inputs that arent clear
    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i, j , k , l = diplomacy_read(s)
        self.assertEqual(i,  "B")
        self.assertEqual(j, "Barcelona")
        self.assertEqual(k, "Move")
        self.assertEqual(l,  "Madrid")

    # fail case
    def test_read_3(self):
        s = "C Paris Support D\n"
        i, j , k ,l  = diplomacy_read(s)
        self.assertEqual(i, "C")
        self.assertEqual(j, "Paris")
        self.assertEqual(k, "Support")
        self.assertEqual(l, "D")


    # ----
    # solve
    # ----

    def test_solve_1(self):
        v = diplomacy_solve([['A','Madrid','Hold']])
        self.assertEqual(v, ['A'])

    def test_solve_2(self):
        v = diplomacy_solve([['A','Madrid','Hold'],['B','Barcalona','Move','Madrid']])
        self.assertEqual(v, [])

    def test_solve_3(self):
        v = diplomacy_solve([['A','Madrid','Hold'],['B','Barcalona','Move','Madrid'], ['C' , "London", 
        'Support', "B"]])
        self.assertEqual(v, ['B','C'])

    def test_solve_4(self):
        v = diplomacy_solve([['A','Madrid','Hold'],['B','Barcalona','Move','Madrid'], ['C' , "London", 
        'Support', "B"],['D','Austin','Move','London']])
        self.assertEqual(v, [])

    

    
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print([['A','Madrid','Hold']], ['A'] , w )
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print([['A','Madrid','Hold'],['B','Barcalona','Move','Madrid']], [] , w )
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print([['A','Madrid','Hold'],['B','Barcalona','Move','Madrid']], [] , w )
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    #add more

    # -----
    # final
    # -----

    #1-6 are all given examples still need to do corners cases 
    # and more examples cases
 
    def test_final(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_final(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
    
    def test_final_1(self):
        r = StringIO("A Madrid Hold\nB Barcalona Move Madrid\n")
        w = StringIO()
        diplomacy_final(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_final_3(self):
        r = StringIO("A Madrid Hold\nB Barcalona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_final(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_final_4(self):
        r = StringIO("A Madrid Hold\nB Barcalona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_final(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


    

# ----
# main
# ----


if __name__ == "__main__": #pragma no cover
    main()
